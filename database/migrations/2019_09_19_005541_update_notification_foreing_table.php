<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotificationForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->foreign('id_type_notification')->references('id_type_notification')->('type_notification');
            $table->foreign('id_account')->references('id_account')->('account');
            $table->foreign('id_room')->references('id_room')->('room');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->dropforeign(['id_type_notification']);
            $table->dropforeign(['id_account']);
            $table->dropforeign(['id_room']);
            //
        });
    }
}
