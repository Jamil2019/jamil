<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account', function (Blueprint $table) {
            $table->bigIncrements('id_account');
            $table->string('name_account')->nullable();
            $table->string('active_account')->nullable();
            $table->string('zip_account')->nullable();
            $table->bigInteger('id_time_zone')->unsigned();
            $table->bigInteger('id_communication')->unsigned();
            $table->bigInteger('id_state')->unsigned();
            $table->bigInteger('id_contry')->unsigned();
            $table->bigInteger('id_type_account')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account');
    }
}
